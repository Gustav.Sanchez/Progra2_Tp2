package prog2.tp2_2017a;


import java.util.ArrayList;
import java.util.List;

public class TrieChar<V>
{
	private Nodo<V> raiz;
	private Alfabeto<Character> alf;

	public TrieChar(Alfabeto<Character> alf) 
	{
		this.raiz=new Nodo<V>(alf.tam());
		this.alf = alf;
	}
	
	public void agregar(String clave, V valor) 
	{
	
		Nodo<V> n = raiz;
		
		int i = 0;
		
		while(i<clave.length())
		{
			if (n.hijo(alf.indice(clave.charAt(i))) == null)
			{
				n.setHijo(alf.indice(clave.charAt(i)), new Nodo<V>(alf.tam()));
				n = n.hijo(alf.indice(clave.charAt(i)));
			}
			
			else
				n = n.hijo(alf.indice(clave.charAt(i)));
			
			i++;
		}
		
		n.val = valor;
	}
	
	public V obtener(String clave) 
	{
				
		Nodo<V> n = raiz;
		
		int i = 0;
		 
		while(i<clave.length())
		{
			if (n.hijo(alf.indice(clave.charAt(i))) != null)
				n = n.hijo(alf.indice(clave.charAt(i)));

			else
				return null;
			
			i++;
		}
	
		return n.val;
	}

	public List<V> busqueda(String prefijo) 
	{	
		
		ArrayList<V> lista=new ArrayList<V>();
		Nodo<V> n=raiz;			
		int i=0;
		
		while (i<prefijo.length())
		{				
			if (n.hijo(alf.indice(prefijo.charAt(i)))!=null)
			{					
				n = n.hijo(alf.indice(prefijo.charAt(i)));
				i++;					
			}
			
			else
				return lista;			
		}
		
		
		busqueda(n,lista);
		return  lista;			
				
	}

	private void busqueda(Nodo<V> n, ArrayList<V> lista) 
	{
		for (int i=0; i<alf.tam();i++)
		{
			if (n.val!=null & !lista.contains(n.val))
				lista.add(n.val);

			if (n.hijo(i)!=null)
				busqueda(n.hijo(i),lista);			
		}
	}

	public void eliminar(String clave)
	{
		Nodo<V> anterior=null;	
		Nodo<V> n=raiz;		
		
		int i=0;
		
		
		while (i<clave.length())
		{		
			if (n.hijo(alf.indice(clave.charAt(i)))!=null)
			{	
				anterior=n;
				n = n.hijo(alf.indice(clave.charAt(i)));
				i++;					
			}
			
			else
				throw new RuntimeException("clave inexistente");					
		}
		
		n.val=null;
		
		if (n.esHoja())
		{
			if (i == 0)
				return;
			
			else
				anterior.setHijo(alf.indice(clave.charAt(i-1)), null);
		}	
	}

	@Override
	public String toString() 
	{
		ArrayList<String> lista = new ArrayList<>();
		ArrayList<String> usado = new ArrayList<>();
		
		if(raiz==null)
			return null;
	
		toString(raiz,lista, usado,"");
	
		return lista.toString();		
	}


	private ArrayList<String> toString(Nodo<V> raiz, ArrayList<String> lista, ArrayList<String> usado, String clave)
	{
		for (int i = 0; i<alf.tam(); i++)
		{
			if (raiz.val != null && !usado.contains(clave))
			{
				lista.add("Prefijo: " + clave + " Valor: " + raiz.val);
				usado.add(clave);
			}
				
			if (raiz.hijo(i) != null)
				toString(raiz.hijo(i),lista,usado,clave+ alf.elem(i));
		}
	
		return lista;
	}


	@Override
	public boolean equals(Object o) 
	{
		if(this == null & o==null)
		{
			return true;
		}
		else if (this==null & o!=null)
		{
			return false;
		}
		else if (this!=null & o==null)
		{
			return false;
		}

		if (o.getClass()==this.getClass())
		{
			TrieChar<?> nuevoTrie =(TrieChar<?>)o;

			if (nuevoTrie.alf.getClass()==this.alf.getClass())
				return this.raiz.equals(nuevoTrie.raiz);	
		}

		return false;
	}
}
