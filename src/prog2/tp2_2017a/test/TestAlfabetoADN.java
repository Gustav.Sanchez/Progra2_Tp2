package prog2.tp2_2017a.test;

import static org.junit.Assert.*;

import org.junit.Test;

import prog2.tp2_2017a.Alfabeto;
import prog2.tp2_2017a.AlfabetoADN;
import prog2.tp2_2017a.Base;

public class TestAlfabetoADN 
{

	Alfabeto<Base> alf = new AlfabetoADN();
	
	@Test
	public void testA() 
	{
		assertEquals(0, alf.indice(Base.A));
	}
	
	@Test
	public void testC() 
	{
		assertEquals(1, alf.indice(Base.C));
	}
	
	@Test
	public void testG() 
	{
		assertEquals(2, alf.indice(Base.G));
	}
	
	@Test
	public void testT() 
	{
		assertEquals(3, alf.indice(Base.T));
	}
	
	@Test
	public void testTam() 
	{
		assertEquals(4, alf.tam());
	}
}