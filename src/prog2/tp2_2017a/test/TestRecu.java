package prog2.tp2_2017a.test;

import prog2.tp2_2017a.Digitos;
import prog2.tp2_2017a.Palabras;
import prog2.tp2_2017a.TrieChar;

public class TestRecu 
{
	public static void main(String[] args) 
	{
		// Ejemplos del pdf
		System.out.println("Test con otro alfabeto del ejemplo del pdf".toUpperCase());
		TrieChar<String> t1 = new TrieChar<>(new Digitos());
		TrieChar<String> t2 = new TrieChar<>(new Digitos());
		
		
		
		t1.agregar("1232", "casa");
		t1.agregar("1233", "cosa");
		t1.agregar("1234", "coso");
		t1.agregar("123", "caso");
		
		t2.agregar("1232", "casa");
		t2.agregar("1233", "cosa");
		t2.agregar("1234", "coso");
		t2.agregar("123", "caso");
		
		System.out.println(t1);
		System.out.println(t2);
		System.out.println(t1.equals(t2));
		
		t2.eliminar("123");
		
		System.out.println(t1);
		System.out.println(t2);		
		System.out.println(t1.equals(t2));
		
		//eliminar hojas
		System.out.println("Eliminar una hoja".toUpperCase());
		
		System.out.println(t1);
		t1.eliminar("1234");
		System.out.println(t1);
		
		//eliminar TrieChar con unicaente raiz
		System.out.println("Eliminar una unica clave que se encuentra en un arbol con solo una raiz".toUpperCase());
		TrieChar<String> t3 = new TrieChar<>(new Digitos());
		t3.agregar("", "perro");
		System.out.println(t3);
		t3.eliminar("");
		System.out.println(t3);
		
		
		//eliminar valor de un TrieChar que no posea solo una raiz o eliminar una hoja
		
		System.out.println("eliminar valor de un TrieChar que no posea solo una raiz o eliminar una hoja".toUpperCase());
		
		TrieChar<String> t4 = new TrieChar<>(new Palabras());
		
		t4.agregar("a", "loro");
		t4.agregar("ab", "panda");
		t4.agregar("b", "oso");
		t4.agregar("z", "mono");
		t4.agregar("ta", "perroo");
		t4.agregar("d", "gato");
		t4.agregar("ca", "caballo");
		
		System.out.println(t4);
		
		t4.eliminar("a");
		System.out.println(t4);
		
		
		//eliminar un valor inexistente	
		
		System.out.println("eliminar una clave que no existe".toUpperCase());
		t4.eliminar("");
		System.out.println(t4);
	}
}