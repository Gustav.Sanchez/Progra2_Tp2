package prog2.tp2_2017a;

import java.util.Arrays;

class Nodo<V>
{
	V val;
	private Nodo<V> hijos[];

	@SuppressWarnings("unchecked")
	Nodo(int tam) 
	{
		hijos = new Nodo[tam];
	}

	Nodo<V> hijo(int i) 
	{
		return hijos[i];
	}

	void setHijo(int i, Nodo<V> hijo) 
	{
		hijos[i] = hijo;
	}

	@Override
	public String toString() 
	{
		return  val.toString();
	}

	@Override
	public boolean equals(Object o) 
	{
		if(this == null & o==null)
		{
			return true;
		}
		else if (this==null & o!=null)
		{
			return false;
		}
		else if (this!=null & o==null)
		{
			return false;
		}
		
		if (this.getClass() != o.getClass())
			return false;
		
		Nodo<?> n = (Nodo<?>) o;
		
		if (this.hijos.length != n.hijos.length)
			return false;
		
		if (!Arrays.equals(hijos, n.hijos))
			return false;
		
		if (val == null) 
		{
			if (n.val != null)
				return false;
		} 
		
		else if (!val.equals(n.val))
			return false;
		
		return true;
	}

	public boolean esHoja() 
	{
		for (Nodo<?> n: hijos)
		{
			if (n!=null)
				return false;
		}
		return true;
	}
}
