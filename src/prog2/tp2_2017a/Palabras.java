package prog2.tp2_2017a;

public class Palabras  implements Alfabeto<Character>
{	
	@Override
	public int tam() 
	{ 
		return 26; 
	}

	@Override
	public int indice(Character c) 
	{
		if (c >= 'a' && c <= 'z')
			return c - 'a';

		throw new RuntimeException("digito no v�lido: " + c);
	}

	@Override
	public Character elem(int n) 
	{
		int res = n + 97;

		if(n>=0 && n<=25)
			return (char) res;
		else
			throw new RuntimeException("indice no v�lido");
	}	
}
