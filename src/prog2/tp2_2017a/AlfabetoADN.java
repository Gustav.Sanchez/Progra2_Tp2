package prog2.tp2_2017a;

public class AlfabetoADN implements Alfabeto<Base>
{

	@Override
	public int tam() 
	{ 
		return 4; 
	}
	
	@Override
	public int indice(Base b) 
	{			
		if (Base.A.equals(b))
			return 0;
		
		else if (Base.C.equals(b))
			return 1;
			
		else if (Base.G.equals(b))
			return 2;
				
		else
			return 3;
	}

	@Override
	public Base elem(int n) 
	{
		if (n==0)
			return Base.A;
		
		else if(n==1)
			return Base.C;
		
		else if(n==2)
			return Base.G;
		
		else if(n==3)
			return Base.T;
		
		else
			throw new RuntimeException("indice no v�lido");
	}	
}
