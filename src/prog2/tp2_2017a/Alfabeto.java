 package prog2.tp2_2017a;

public interface Alfabeto<T>
{
	int tam();

	int indice(T elem);
	
	T elem(int n);
	
}
