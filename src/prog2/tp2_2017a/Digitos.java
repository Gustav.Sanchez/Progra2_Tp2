package prog2.tp2_2017a;

public class Digitos implements Alfabeto<Character>
{
	@Override
	public int tam() 
	{ 
		return 10; 
	}

	@Override
	public int indice(Character c) 
	{
		if (c >= '0' && c <= '9')
			return c - '0';

		throw new RuntimeException("digito no v�lido: " + c);
	}
	
	@Override
	public Character elem(int n) 
	{
		int res = n + 48;
		
		if(n>=0 && n<=9)
			return (char) res;
		else
			throw new RuntimeException("indice no v�lido");
	}	
}